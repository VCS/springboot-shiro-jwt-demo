/**
 * @Title: ResponseBean.java
 * @Package www.codepeople.cn.entity
 * @Description: 
 * Copyright: Copyright (c) 2019 www.codepeople.cn Inc. All rights reserved. 
 * Website: www.codepeople.cn
 * 注意：本内容仅限于海南科澜技术信息有限公司内部传阅，禁止外泄以及用于其他的商业目 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午4:11:44
 * @version V1.0
 */

package www.codepeople.cn.entity;

import lombok.Data;

/**
 * @ClassName: ResponseBean
 * @Description: 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午4:11:44 
 */
@Data
public class ResponseBean {
	/**
	 * http状态码
	 */
	private Integer code;
	/**
	 * 返回信息
	 */
	private String msg;
	/**
	 * 返回数据
	 */
	private Object data;
	
	public ResponseBean() {
		
	}
	
	public ResponseBean(Integer code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
}
