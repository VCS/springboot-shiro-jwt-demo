package www.codepeople.cn.mapper;

import java.util.List;

import www.codepeople.cn.entity.User;

public interface UserMapper {
    int deleteByPrimaryKey(String userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

	/**
	 * @Title: getUser
	 * @Description: 根据用户username查询用户信息
	 * @Author 刘仁
	 * @DateTime 2019年4月1日 下午5:13:42
	 * @param username
	 * @return 
	 */
	
	
	
	User getUser(String username);

	/**
	 * @return 
	 * @Title: listUser
	 * @Description: 获取用户列表
	 * @Author 刘仁
	 * @DateTime 2019年4月1日 下午6:29:36 
	 */
	
	
	
	List<User> listUser();
}