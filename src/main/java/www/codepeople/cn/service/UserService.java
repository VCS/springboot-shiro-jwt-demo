/**
 * @Title: UserService.java
 * @Package www.codepeople.cn.service
 * @Description: 
 * Copyright: Copyright (c) 2019 www.codepeople.cn Inc. All rights reserved. 
 * Website: www.codepeople.cn
 * 注意：本内容仅限于海南科澜技术信息有限公司内部传阅，禁止外泄以及用于其他的商业目 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午5:01:44
 * @version V1.0
 */

package www.codepeople.cn.service;

import java.util.List;

import www.codepeople.cn.entity.User;

/**
 * @ClassName: UserService
 * @Description: 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午5:01:44 
 */

public interface UserService {

	/**
	 * @Title: getUser
	 * @Description: 根据username查询用户信息
	 * @Author 刘仁
	 * @DateTime 2019年4月1日 下午5:12:00
	 * @param username
	 * @return 
	 */
	
	
	
	User getUser(String username);

	/**
	 * @return 
	 * @Title: listUser
	 * @Description: 
	 * @Author 刘仁
	 * @DateTime 2019年4月1日 下午6:27:57 
	 */
	
	
	
	List<User> listUser();

}
