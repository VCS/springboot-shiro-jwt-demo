/**
 * @Title: UserServiceImpl.java
 * @Package www.codepeople.cn.service.impl
 * @Description: 
 * Copyright: Copyright (c) 2019 www.codepeople.cn Inc. All rights reserved. 
 * Website: www.codepeople.cn
 * 注意：本内容仅限于海南科澜技术信息有限公司内部传阅，禁止外泄以及用于其他的商业目 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午5:02:16
 * @version V1.0
 */

package www.codepeople.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import www.codepeople.cn.entity.User;
import www.codepeople.cn.mapper.UserMapper;
import www.codepeople.cn.service.UserService;

/**
 * @ClassName: UserServiceImpl
 * @Description: 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午5:02:16 
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	/*
	 * <p>Title: getUser</p>
	 * <p>Description: 根据用户username查询用户信息</p>
	 * @Author 刘仁
	 * @DateTime 2019年4月1日 下午5:12:24
	 * @param username
	 * @return 
	 * @see www.codepeople.cn.service.UserService#getUser(java.lang.String) 
	 */
	
	
	@Override
	public User getUser(String username) {
		return userMapper.getUser(username);
	}


	/*
	 * <p>Title: listUser</p>
	 * <p>Description:获取用户列表 </p>
	 * @Author 刘仁
	 * @DateTime 2019年4月1日 下午6:28:06 
	 * @see www.codepeople.cn.service.UserService#listUser() 
	 */
	
	
	@Override
	public List<User> listUser() {
		return userMapper.listUser();
	}

}
