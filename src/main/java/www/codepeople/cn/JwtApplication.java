/**
 * @Title: JwtApplication.java
 * @Package www.codepeople.cn
 * @Description: 
 * Copyright: Copyright (c) 2019 www.codepeople.cn Inc. All rights reserved. 
 * Website: www.codepeople.cn
 * 注意：本内容仅限于海南科澜技术信息有限公司内部传阅，禁止外泄以及用于其他的商业目 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午5:26:30
 * @version V1.0
 */

package www.codepeople.cn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName: JwtApplication
 * @Description: 
 * @Author 刘仁
 * @DateTime 2019年4月1日 下午5:26:30 
 */
@SpringBootApplication
@MapperScan("www.codepeople.cn.mapper.**")
public class JwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtApplication.class, args);
	}
}
